#define _POSIX_C_SOURCE 199309L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <poll.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>

#include <X11/Xlib.h>
#include "common.h"

#define MAXLEN 1024

struct Conn {
	int fd;
	char path[MAXLEN];
	char status[MAXLEN];    /* zero-terminated status string */
	char buf[MAXLEN];
	int bufp;
};

void sigrecv(int sig);
void setstatus(void);
int update(struct Conn *conn);

#define STATUSLEN 80
const char *ellipsis = "...";

static Display *disp;
static struct Conn *conns;
static int nconns = 0;

static int interrupted = 0;

int
main(int argc, char* argv[])
{
	int len, i;
	int st = 0;
	char *home = getenv("HOME");
	char path[MAXLEN];
	struct pollfd *fds;
	struct sigaction sact;
	
	sact.sa_sigaction = NULL;
	sact.sa_handler = sigrecv;
	sigemptyset(&sact.sa_mask);
	sact.sa_flags = 0;
	
	sigaction(SIGTERM, &sact, NULL);
	sigaction(SIGINT, &sact, NULL);
	
	if (!home)
		die("HOME not set");
	len = snprintf(path, MAXLEN, "%s/.astatusbar", home);
	if (len >= MAXLEN)
		die("path too long");
		
	if (argc == 1)
		return 0;
	
	nconns = argc - 1;
	conns = malloc(sizeof(struct Conn) * (argc-1));
	fds = malloc(sizeof(struct pollfd) * (argc-1));
	if (!conns || !fds)
		die("failed to allocate memory");
	
	disp = XOpenDisplay(0);
	if (!disp)
		die("failed to open display");
	
	for (i = 0; i < nconns; i++) {
		conns[i].bufp = 0;
		conns[i].status[0] = 0;
		len = snprintf(conns[i].path, MAXLEN, "%s/.astatusbar/%s", home, argv[i+1]);
		if (len >= MAXLEN)
			die("path too long");
		if (mkfifo(conns[i].path, 0600))
			die("%s: can't create fifo:", conns[i].path);
		conns[i].fd = fds[i].fd = open(conns[i].path, O_RDWR);
		if (fds[i].fd < 0)
			die("can't open fifo:");
		fds[i].events = POLLIN;
	}
	
	for (;;) {
		if (poll(fds, nconns, -1) == -1) {
			if (errno != EINTR)
				die("poll failed:");
		}
		if (interrupted)
			break;
		
		for (i = 0; i < nconns; i++) {
			if (! fds[i].revents & POLLIN)
				continue;
			if (update(&conns[i]))
				die("%s:", conns[i].path);
		}
		
		setstatus();
	}
	
	for (i = 0; i < nconns; i++) {
		close(conns[i].fd);
		unlink(conns[i].path);
	}
	XCloseDisplay(disp);
	return st;
}

void
sigrecv(int sig)
{
	interrupted = 1;
}

int
update(struct Conn *conn)
{
	int n;
	char *nstart, *end, *c;
	
	n = read(conn->fd, conn->buf + conn->bufp, MAXLEN - conn->bufp);
	if (n < 0)
		return n;
	end = conn->buf + conn->bufp + n;
	
	nstart = conn->buf;
	while (end - nstart > 0 && (c = memchr(nstart, '\n', end - nstart))) {
		memmove(conn->status, nstart, c-nstart);
		conn->status[c-nstart] = 0;
		if (c-nstart > STATUSLEN)
			strcpy(conn->status + STATUSLEN - strlen(ellipsis), ellipsis);
		nstart = c + 1;
	}
	
	if (nstart != conn->buf) {
		memmove(conn->buf, nstart, end - nstart);
		conn->bufp = end - nstart;
	}
	return 0;
}

void
setstatus(void)
{
	int i, len;
	char sline[MAXLEN]= {0, };
	char *p = sline;
	
	for (i = 0; i < nconns; i++) {
		if (conns[i].status[0] == 0)
			continue;
		*p++ = ' ';
		len = strlen(conns[i].status);
		if (p - sline + len + 1 >= MAXLEN)
			break;
		memmove(p, conns[i].status, len);
		p += len;
	}
	*p = 0;
	if (!XStoreName(disp, DefaultRootWindow(disp), sline))
		die("failed to set root window name");
	XFlush(disp);
}
