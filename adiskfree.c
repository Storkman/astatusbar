#include <stdio.h>
#include <stdarg.h>
#include <sys/statvfs.h>
#include "common.h"

int
main(int argc, char *argv[])
{
	struct statvfs fs;
	
	if (argc != 2) {
		fprintf(stderr, "usage: %s [mounted disk path]\n", argv[0]);
		return 1;
	}
	
	if (statvfs(argv[1], &fs))
		die("%s: can't read filesystem info:", argv[1]);
	printf("[HDD: %.1f GB]\n", 1.0 * fs.f_bavail * fs.f_bsize / (1024*1024*1024));
	return 0;
}
